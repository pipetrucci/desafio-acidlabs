# Desafío Acid Labs

Aplicación web que sincroniza cada 10 segundos 
la temperatura y la hora de 5 ciudades alrededor del mundo.

Técnologías usadas:

* Ruby 2.5.0p0
* Rails 5.2.2
* Bootstrap 4.3.1
* Jquery 3.x
* Redis for Action Cable 
* Sidekiq for Active Jobs
* Dark Sky API (Forecast Request)

Demo de la aplicación (Heroku)
https://acid-labs-ffabio.herokuapp.com/