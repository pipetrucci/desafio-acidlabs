# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

App.forecast = App.cable.subscriptions.create "ForecastComunicationChannel",
  run_job: ->
    @perform('worker')
  received: (data) ->
    $('#messages').empty().append data['message']

$ ->
  setInterval =>
    App.forecast.run_job()
  , 10000