class ForecastComunicationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "notificaction_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def worker
    ForecastJob.perform_later
  end

end
