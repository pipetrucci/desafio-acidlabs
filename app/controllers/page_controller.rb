class PageController < ApplicationController
  before_action :fetch_cities

  def index
    @results = helpers.get_forecast_result
  end

  private
  def fetch_cities
    helpers.set_cities
  end
end
