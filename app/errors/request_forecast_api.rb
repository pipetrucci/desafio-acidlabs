class RequestForecastApi
  # Domain specific errors
  attr_reader :name
  class RequestError < StandardError
    def initialize(name)
      @name = name
    end

    def message
      "How unfortunate! The API Request Failed When Trying Request for #{@name} City"
    end
  end

  def self.handle_probability_error(name)
    raise RequestError, name  if (rand() < 0.1)
  end
end