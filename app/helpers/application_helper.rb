module ApplicationHelper
  def get_forecast_result
    cities = %w(CL CH NZ AU UK USA)
    results = []

    cities.each do |city|
      data = $redis.hgetall city
      unless data.nil?
        loop do
          result = ForecastIO.forecast(data['latitude'], data['longitude'], params: { units: 'si', exclude: 'minutely, hourly, daily, alerts, flags' })
          begin
            RequestForecastApi.handle_probability_error(data['name'])
          rescue RequestForecastApi::RequestError => e
            $redis.hset 'api.errors', Time.now, e.message
            next
          end
          unless result.nil?
            results << {name: data['name'], temperature: result.currently.temperature, time: Time.at(result.currently.time).in_time_zone(result.timezone).strftime("%T")}
            break
          end
        end
      end
    end
    results
  end
end
