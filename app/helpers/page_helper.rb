module PageHelper
  def set_cities
    $redis.hmset 'CL', :name, "Santiago", :latitude, "-33.45694", :longitude, "-70.64827"
    $redis.hmset 'CH', :name, "Zurich", :latitude, "47.36667", :longitude, "8.55"
    $redis.hmset 'NZ', :name, "Auckland", :latitude, "-36.86667", :longitude, "174.76667"
    $redis.hmset 'AU', :name, "Sydney", :latitude, "-33.86785", :longitude, "151.20732"
    $redis.hmset 'UK', :name, "Londres", :latitude, "51.51279", :longitude, "-0.09184"
    $redis.hmset 'USA', :name, "Georgia", :latitude, "32.75042", :longitude, "-83.50018"
  end
end
