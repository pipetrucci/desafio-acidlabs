class ForecastJob < ApplicationJob
  include ApplicationHelper
  queue_as :default

  def perform(*args)
    # Do something later
    results = get_forecast_result
    ActionCable.server.broadcast 'notificaction_channel', message: render_message(results)
  end

  private
  def render_message(message)
    PageController.render(partial: 'page/info_cities', locals: { cities: message })
  end
end
