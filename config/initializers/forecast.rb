require 'forecast_io'

ForecastIO.configure do |c|
  c.api_key = Rails.application.credentials.forecast_secret_key
end